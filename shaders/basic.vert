#version 330

uniform vec2 uniform_position_offset;

in vec3 pos;

void main()
{
    gl_Position = vec4(pos.x + uniform_position_offset.x, pos.y + uniform_position_offset.y, pos.z, 1.0);
}
