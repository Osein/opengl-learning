#version 330

uniform vec4 uniform_vert_color;

out vec4 frag_color;

void main()
{
   frag_color = uniform_vert_color;
}
