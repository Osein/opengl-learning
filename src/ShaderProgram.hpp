//
//  ShaderProgram.hpp
//  OpenGLLearning
//
//  Created by Osein on 4.08.2018.
//  Copyright © 2018 Osein. All rights reserved.
//

#ifndef ShaderProgram_hpp
#define ShaderProgram_hpp

#include <GLFW/glfw3.h>
#include <string>
#include <map>

#include "../includes/glm/glm.hpp"

class ShaderProgram
{
public:
    
    ShaderProgram();
    ~ShaderProgram();
    
    enum ShaderType
    {
        VERTEX,
        FRAGMENT,
        PROGRAM
    };
    
    bool LoadShaders(const GLchar * vsFileName, const GLchar * fsFileName);
    void use();
    
    void setUniform(const GLchar * cpUniformName, const glm::vec2 & elem);
    void setUniform(const GLchar * cpUniformName, const glm::vec3 & elem);
    void setUniform(const GLchar * cpUniformName, const glm::vec4 & elem);
    
private:
    
    void checkCompileErrors(GLuint shader, ShaderType shaderType);
    std::string GetFileAsString(const std::string & fileName);
    GLint getUniformLocation(const GLchar * uniformName);
    
    GLuint m_iShaderProgram;
    
    std::map<std::string, GLint> mUniformLocations;
    
};

#endif /* ShaderProgram_hpp */
