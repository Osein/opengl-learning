//
//  main.cpp
//  OpenGLLearning
//
//  Created by Osein on 18.07.2018.
//  Copyright © 2018 Osein. All rights reserved.
//

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <sstream>

#include "ShaderProgram.hpp"

const char * m_cpAppTitle = "Learning OpenGL 4";
const int m_iWindowWidth = 800;
const int m_iWindowHeight = 600;
const int m_iFullScreen = false;
int m_iWireframe = false;
GLFWwindow * m_pWindow = nullptr;

static void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    } else if(key == GLFW_KEY_W && action == GLFW_PRESS) {
        m_iWireframe = !m_iWireframe;
        if(m_iWireframe) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        } else {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }
}

static void showFps(GLFWwindow * window) {
    static double previousSeconds = 0.0;
    static int frameCount = 0;
    double elapsedSeconds;
    double currentSeconds = glfwGetTime();
    
    elapsedSeconds = currentSeconds - previousSeconds;
    
    if(elapsedSeconds > 0.25) {
        previousSeconds = currentSeconds;
        double fps = (double) frameCount / elapsedSeconds;
        double msPerFrame = 1000.0 / fps;
        
        std::ostringstream outs;
        outs.precision(3);
        outs << std::fixed
        << m_cpAppTitle << " "
        << "FPS: " << fps << " "
        << "Frame Time: " << msPerFrame << " (ms)";
        glfwSetWindowTitle(window, outs.str().c_str());
        
        frameCount = 0;
    }
    
    frameCount++;
}

bool initOpenGL()
{
    if (!glfwInit()) {
        std::cerr << "Failed to init GLFW." << std::endl;
        return false;
    }
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    
    GLFWmonitor * m_pMonitor = glfwGetPrimaryMonitor();
    const GLFWvidmode * m_pVidMode = glfwGetVideoMode(m_pMonitor);
    
    if(m_iFullScreen && m_pVidMode != nullptr) {
        m_pWindow = glfwCreateWindow(m_pVidMode->width, m_pVidMode->height, m_cpAppTitle, m_pMonitor, nullptr);
    } else {
        m_pWindow = glfwCreateWindow(m_iWindowWidth, m_iWindowHeight, m_cpAppTitle, nullptr, nullptr);
    }
    
    if (m_pWindow == nullptr) {
        std::cerr << "Failed to create GLFW window." << std::endl;
        glfwTerminate();
        return false;
    }
    
    glfwSetKeyCallback(m_pWindow, keyCallback);
    glfwMakeContextCurrent(m_pWindow);
    glewExperimental = GL_TRUE;
    if(glewInit() != GLEW_OK) {
        std::cerr << "Glew init failed." << std::endl;
        glfwTerminate();
        return false;
    }
    
    /* get version info */
    const GLubyte * renderer = glGetString( GL_RENDERER );
    const GLubyte * version = glGetString( GL_VERSION );
    
    printf( "Renderer: %s\n", renderer );
    printf( "OpenGL version supported %s\n", version );
    
    const char * glfwVersion = glfwGetVersionString();
    
    printf( "GLFW version supported %s\n", glfwVersion );
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
//    glEnable(GL_DEPTH_TEST);
//    glDepthFunc(GL_LESS);
    
    return true;
}

int main(int argc, const char* argv[]) {
    if(!initOpenGL()) {
        std::cerr << "Failed to init OpenGL." << std::endl;
        return -1;
    }
    
    GLfloat vertices[] = {
        -0.5f, 0.5f, 0.0f,
        0.5f, 0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f
    };
    
    GLuint indices[] {
        0, 1, 2,
        0, 2, 3
    };

    GLuint vertexBuffer, indexBuffer, vertexArray;
    
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glGenVertexArrays(1, &vertexArray);
    glBindVertexArray(vertexArray);
    
    // position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0 , nullptr);
    glEnableVertexAttribArray(0);
    
    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    
    ShaderProgram shaderProgram;
    shaderProgram.LoadShaders("shaders/basic.vert", "shaders/basic.frag");
    
    while(!glfwWindowShouldClose(m_pWindow)) {
        glfwPollEvents();
        
        glClear(GL_COLOR_BUFFER_BIT);
        showFps(m_pWindow);
        
        shaderProgram.use();
        
        GLfloat time = glfwGetTime();
        GLfloat blueColor = (sin(time) / 2) + 0.5f;
        
        glm::vec2 pos;
        
        pos.x = sin(time) / 2;
        pos.y = sin(time) / 2;
        
        shaderProgram.setUniform("uniform_vert_color", glm::vec4(0.0f, 0.0f, blueColor, 1.0f));
        shaderProgram.setUniform("uniform_position_offset", pos);
        
        glBindVertexArray(vertexArray);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
        
        glfwSwapBuffers(m_pWindow);
    }
    
    glDeleteVertexArrays(1, &vertexArray);
    glDeleteBuffers(1, &vertexBuffer);
    glDeleteBuffers(1, &indexBuffer);
    
    glfwTerminate();
    
    return 0;
}
