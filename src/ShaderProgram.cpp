//
//  ShaderProgram.cpp
//  OpenGLLearning
//
//  Created by Osein on 4.08.2018.
//  Copyright © 2018 Osein. All rights reserved.
//

#include "ShaderProgram.hpp"
#include <fstream>
#include <iostream>
#include <sstream>

ShaderProgram::ShaderProgram() : m_iShaderProgram(0)
{
    mUniformLocations.clear();
}

ShaderProgram::~ShaderProgram()
{
    glDeleteProgram(m_iShaderProgram);
}

bool ShaderProgram::LoadShaders(const GLchar * vsFileName, const GLchar * fsFileName)
{
    std::string vsString = GetFileAsString(vsFileName);
    const GLchar * cpVertexShader = vsString.c_str();
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &cpVertexShader, nullptr);
    glCompileShader(vertexShader);
    checkCompileErrors(vertexShader, ShaderType::VERTEX);
    
    std::string fsString = GetFileAsString(fsFileName);
    const GLchar * cpFragmentShader = fsString.c_str();
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &cpFragmentShader, nullptr);
    glCompileShader(fragmentShader);
    checkCompileErrors(vertexShader, ShaderType::FRAGMENT);
    
    m_iShaderProgram = glCreateProgram();
    glAttachShader(m_iShaderProgram, vertexShader);
    glAttachShader(m_iShaderProgram, fragmentShader);
    glLinkProgram(m_iShaderProgram);
    checkCompileErrors(m_iShaderProgram, ShaderType::PROGRAM);
    
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    
    return true;
}

void ShaderProgram::use()
{
    if(m_iShaderProgram > 0) {
        glUseProgram(m_iShaderProgram);
    }
}

void ShaderProgram::checkCompileErrors(GLuint shader, ShaderType shaderType)
{
    int status = 0;
    
    if(shaderType == ShaderType::PROGRAM) {
        glGetProgramiv(shader, GL_LINK_STATUS, &status);
        
        if(status == GL_FALSE) {
            GLint messageLength = 0;
            glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &messageLength);
            
            std::string errorStr(messageLength, ' ');
            glGetProgramInfoLog(shader, messageLength, &messageLength, &errorStr[0]);
            std::cerr << "Shader Program link error. Message: " << errorStr << std::endl;
        }
    } else {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
        
        if(status == GL_FALSE) {
            GLint messageLength = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &messageLength);
            
            std::string errorStr(messageLength, ' ');
            glGetShaderInfoLog(shader, messageLength, &messageLength, &errorStr[0]);
            std::cerr << "Shader compile error. Message: " << errorStr << std::endl;
        }
    }
}

std::string ShaderProgram::GetFileAsString(const std::string & fileName)
{
    std::stringstream strStream;
    std::ifstream fStream;
    
    try {
        fStream.open(fileName, std::ios::in);
        
        if(!fStream.fail()) {
            strStream << fStream.rdbuf();
        } else {
            throw new std::exception;
        }
        
        fStream.close();
    } catch (std::exception e) {
        std::cerr << "Error reading shader(" << fileName << ")" << std::endl;
    }
    
    return strStream.str();
}

GLint ShaderProgram::getUniformLocation(const GLchar * uniformName)
{
    std::map<std::string, GLint>::iterator it = mUniformLocations.find(uniformName);
    
    if(it == mUniformLocations.end())
    {
        mUniformLocations[uniformName] = glGetUniformLocation(m_iShaderProgram, uniformName);
    }
    
    return mUniformLocations[uniformName];
}

void ShaderProgram::setUniform(const GLchar * cpUniformName, const glm::vec2 & elem)
{
    GLint loc = getUniformLocation(cpUniformName);
    glUniform2f(loc, elem.x, elem.y);
}

void ShaderProgram::setUniform(const GLchar * cpUniformName, const glm::vec3 & elem)
{
    GLint loc = getUniformLocation(cpUniformName);
    glUniform3f(loc, elem.x, elem.y, elem.z);
}

void ShaderProgram::setUniform(const GLchar * cpUniformName, const glm::vec4 & elem)
{
    GLint loc = getUniformLocation(cpUniformName);
    glUniform4f(loc, elem.x, elem.y, elem.z, elem.w);
}
